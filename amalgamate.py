
import os
import re
import fileinput


class amalgamate():
    license_start=str('/'+'*'*75)
    license_end=str('*'*76+'/')
    fwd_decl=[]
    gitcommit=os.popen('git log -n 1 --pretty=format:"%H %ai"').read()    
    
    def __init__(self,file):
        print "-->",file
        self.filein=file
        self.fileout=os.path.splitext(os.path.basename(file))[0]
        self.fin=open(file,'r')
        self.fout=open(self.fileout,'w')
        for line in self.fin.readlines():
            if line.startswith("//amalgamation:"):
                command=line.split()[1]
                args=' '.join(line.split()[2:])
                if (command=="add"):
                    self.add_file(args)
                elif (command=="place"):
                    if args == "implementation includes" :
                        self.add_implementation()
                    elif args == "header includes" :
                        self.add_header_includes()
                    elif args == "forward declarations" :
                        self.fout.write(line)
                    else:
                        print "unknown:",args
                elif (command=="include"):
                    if args == "end" :
                        self.add_include_end()
                        
            else:
                self.fout.write(line)
                
        self.fin.close()
        self.fout.close()
        self.add_forward_decl()
                
    
    def add_implementation(self):
        ''''''
        print "add_implementation ?"
        
    def add_include_end(self):
        ''''''
        print "add_include end ?"
        
    def add_header_includes(self):
        ''''''
        print "add_header_includes ?"

    def add_forward_decl(self):
        ''''''
        if len(self.fwd_decl)>0 :
            for line in fileinput.FileInput(self.fileout,inplace=1):
                if "//amalgamation: place forward declarations" in line:
                    line='';
                    for cls in self.fwd_decl:
                        line+=cls
                print line,
            
            
    def add_file(self,fnamein):
        ''''''
        found=False
        print "\t",fnamein
        for root, dirs, files in os.walk(os.path.dirname(self.filein)):
            for file in files:
                fname=os.path.join(root, file)
                if fname.endswith(fnamein):
                    found=True
                    fin=open(fname,'r')
                    license=False
                    self.fout.write("\n/* including file '"+file+"'"+" "*(57-len(fname))+'*/\n')
                    self.fout.write("/* commit %s */\n"%self.gitcommit)
                    ifdef = os.path.basename(fname).upper().replace('.','_').replace('-','_')
                    for line in fin.readlines():
                        if line.startswith('// amalgamation:'):
                            print "\t\tunknown", line[:-1]
                        elif line.startswith(self.license_start):
                            license=True
                        elif line.startswith(self.license_end):
                            license=False
                        elif re.match(r'class (\S+);',line):
                            if not line in self.fwd_decl:
                                self.fwd_decl.append(line)
                        elif not line.startswith('#include "'):
                            if not license: 
                                if not ifdef in line:
                                    self.fout.write(line)
                        elif line.strip().endswith('.cpp"'):
                            new_found=line.split('"')[1]
                            self.add_file(new_found)
                                 
                    self.fout.write("/* end of '%s' */"%file)
        if not found:
            print "not found", fnamein



script_dir=os.path.dirname(os.path.realpath(__file__))
for root, dirs, files in os.walk(os.path.join(script_dir,'src')):
    for file in files:
        if file.endswith(".skeleton"):
             amalgamate(os.path.join(root, file))
             



